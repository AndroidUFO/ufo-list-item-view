package com.androidufo.livsample;

import androidx.annotation.ColorRes;
import androidx.annotation.DimenRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.androidufo.listitem.UFOComplexListItemView;
import com.androidufo.listitem.bg.BgBuilder;
import com.androidufo.listitem.bg.StateBgBuilder;
import com.androidufo.listitem.listener.OnItemClickListener;
import com.androidufo.listitem.UFOSimpleUFOListItemView;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private Handler mHandler = new Handler(Looper.getMainLooper());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.slivAlpha).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toast("点击效果改变透明度");
            }
        });

        findViewById(R.id.slivState).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toast("点击效果切换状态颜色");
            }
        });

        findViewById(R.id.slivRipple).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toast("点击效果为水波纹");
            }
        });

        itemFriends();
        itemCamera();
        itemPhotos();
        itemMusic();
        itemMobile();
        itemJack();

    }

    private void itemJack() {
        UFOComplexListItemView itemView = findViewById(R.id.clivJack);
        itemView.iconCircleMode(true)
                .desc("食屎啦！")
                .picImage(R.drawable.ic_ssl);
    }

    private void itemMobile() {
        UFOComplexListItemView itemView = findViewById(R.id.clivMobile);
        itemView.desc("很多美女联系过你")
                .showRightDot(true)
                .rightDotText("99+");
    }

    private void itemMusic() {
        UFOComplexListItemView itemView = findViewById(R.id.clivMusic);
        itemView.desc("杰伦出新专辑了")
                .picImage(R.drawable.jay)
                .picImageCircleMode(true)
                .picImageBadgeText("News")
                .picImageBadgeTextSize(24)
                .picImageBadgeShowAngle(true);
    }

    private void itemPhotos() {
        UFOComplexListItemView itemView = findViewById(R.id.clivPhoto);
        itemView.picText("周杰伦")
                .picImage(R.drawable.jay);
    }

    private void itemCamera() {
        UFOComplexListItemView itemView = findViewById(R.id.clivCamera);
        itemView.showLeftDot(true)
                .showRightDot(true)
                .setOnItemClickListener(new OnItemClickListener<UFOComplexListItemView>() {
                    @Override
                    public void onItemClick(UFOComplexListItemView listItemView) {
                        toast("我可以防止快速点击");
                    }
                });
    }

    private void itemFriends() {
        UFOComplexListItemView itemView = findViewById(R.id.clivFriends);
        itemView.picImage(R.drawable.ic_panghu);
    }

    private void toast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private int getResDimen(@DimenRes int id) {
        return getResources().getDimensionPixelSize(id);
    }

    private int getResColor(@ColorRes int id) {
        return ContextCompat.getColor(this, id);
    }
}