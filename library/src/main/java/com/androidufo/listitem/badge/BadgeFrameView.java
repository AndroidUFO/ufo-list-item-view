package com.androidufo.listitem.badge;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.annotation.IntDef;

/**
 * @ClassName BadgeFrameView
 * @Description TODO
 * @Author admin
 * @Date 2021/4/6 17:17
 */
public class BadgeFrameView extends ViewGroup {

    private static final int MAX_CHILD = 2;

    private @BadgeGravity
    int mBadgeGravity = BadgeGravity.RIGHT_TOP;
    private BadgeView mBadgeView;
    private boolean mHasBringToFont;

    public BadgeFrameView(Context context) {
        this(context, null);
    }

    public BadgeFrameView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BadgeFrameView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setClipChildren(false);
        setPadding(0, 0, 0, 0);
        addBadgeView(context);
    }

    // 不支持设置padding，实际也不需要，该ViewGroup只是为了将badge套在目标View上
    @Override
    public void setPadding(int left, int top, int right, int bottom) {
        super.setPadding(0, 0, 0, 0);
    }

    private void addBadgeView(Context context) {
        mBadgeView = new BadgeView(context);
        addView(mBadgeView, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
    }

    public BadgeView getBadgeView() {
        return mBadgeView;
    }

    public BadgeFrameView badgeText(String text) {
        mBadgeView.text(text);
        return this;
    }

    public BadgeFrameView show() {
        mBadgeView.show(true);
        return this;
    }

    public BadgeFrameView hide() {
        mBadgeView.show(false);
        return this;
    }

    public BadgeFrameView badgeGravity(@BadgeGravity int gravity) {
        if (mBadgeGravity != gravity) {
            mBadgeGravity = gravity;
            requestLayout();
        }
        return this;
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int childCount = getChildCount();
        if (childCount > MAX_CHILD) {
            throw new RuntimeException(BadgeFrameView.class.getSimpleName()
                    + " can not has more than " + MAX_CHILD + " child view");
        }
        if (childCount == MAX_CHILD) {
            setParentDrawOuter();
            layoutTargetView();
            layoutBadgeView();
        }
    }

    private void layoutTargetView() {
        View targetView = getChildAt(mHasBringToFont ? 0 : 1);
        targetView.layout(0, 0, targetView.getMeasuredWidth(), targetView.getMeasuredHeight());
    }

    private void layoutBadgeView() {
        int width = getMeasuredWidth();
        int height = getMeasuredHeight();
        int badgeWidth = mBadgeView.getMeasuredWidth();
        int badgeHeight = mBadgeView.getMeasuredHeight();
        int left;
        int right;
        int top;
        int bottom;
        if (mBadgeGravity == BadgeGravity.LEFT_TOP) {
            left = -badgeWidth / 2;
            top = -badgeHeight / 2;
            right = badgeWidth / 2;
            bottom = badgeHeight / 2;
        } else if (mBadgeGravity == BadgeGravity.RIGHT_TOP) {
            left = width - badgeWidth / 2;
            top = -badgeHeight / 2;
            right = width + badgeWidth / 2;
            bottom = badgeHeight / 2;
        } else if (mBadgeGravity == BadgeGravity.LEFT_BOTTOM) {
            left = -badgeWidth / 2;
            top = height - badgeHeight / 2;
            right = badgeWidth / 2;
            bottom = height + badgeHeight / 2;
        } else if (mBadgeGravity == BadgeGravity.RIGHT_BOTTOM) {
            left = width - badgeWidth / 2;
            top = height - badgeHeight / 2;
            right = width + badgeWidth / 2;
            bottom = height + badgeHeight / 2;
        } else {
            throw new RuntimeException("mBadgeGravity's value must one of @BadgeGravity");
        }
        mBadgeView.layout(left, top, right, bottom);
        mBadgeView.bringToFront();
        mHasBringToFont = true;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        measureChildren(widthMeasureSpec, heightMeasureSpec);
        int measureTargetIndex = getChildCount() == MAX_CHILD ? (mHasBringToFont ? 0 : 1) : 0;
        View measureTargetView = getChildAt(measureTargetIndex);
        int targetWidth = measureTargetView.getMeasuredWidth();
        int targetHeight = measureTargetView.getMeasuredHeight();
        setMeasuredDimension(targetWidth, targetHeight);
    }

    private void setParentDrawOuter() {
        ViewParent parentView = getParent();
        if (parentView != null) {
            if (parentView instanceof ViewGroup) {
                ((ViewGroup) parentView).setClipChildren(false);
            }
        }
    }

    private static final int BADGE_LT = 0;
    private static final int BADGE_LB = 1;
    private static final int BADGE_RT = 2;
    private static final int BADGE_RB = 3;

    @IntDef({BADGE_LT, BADGE_LB, BADGE_RT, BADGE_RB})
    public @interface BadgeGravity {
        int LEFT_TOP = BADGE_LT;
        int LEFT_BOTTOM = BADGE_LB;
        int RIGHT_TOP = BADGE_RT;
        int RIGHT_BOTTOM = BADGE_RB;
    }
}
