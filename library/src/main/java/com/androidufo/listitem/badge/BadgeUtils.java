package com.androidufo.listitem.badge;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

/**
 * @ClassName BadgeUtils
 * @Description TODO
 * @Author admin
 * @Date 2021/4/7 13:26
 */
public class BadgeUtils {

    public static BadgeFrameView bindBadge(View targetView) {
        if (targetView == null) {
            throw new NullPointerException("targetView is null");
        }
        ViewParent parent = targetView.getParent();
        if (parent == null) {
            throw new RuntimeException("targetView has not parentView, please add to a parentView first");
        }
        if (!(parent instanceof ViewGroup)) {
            throw new RuntimeException("targetView' parentView must instanceof ViewGroup");
        }
        ViewGroup parentView = (ViewGroup) parent;
        BadgeFrameView badgeFrameView = new BadgeFrameView(targetView.getContext());
        // 将target的lp设置给BadgeFrameView
        ViewGroup.LayoutParams oldLp = targetView.getLayoutParams();
        badgeFrameView.setLayoutParams(oldLp);
        // 获取targetView在parentView中的索引
        int targetIndex = parentView.indexOfChild(targetView);
        // 移除targetView并添加BadgeFrameView
        parentView.removeView(targetView);
        parentView.addView(badgeFrameView, targetIndex);
        // BadgeFrameView添加targetView
        ViewGroup.LayoutParams targetLp = new ViewGroup.LayoutParams(oldLp.width, oldLp.height);
        badgeFrameView.addView(targetView, targetLp);
        return badgeFrameView;
    }

}
