package com.androidufo.listitem.badge;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.ColorInt;
import androidx.annotation.Nullable;

public class BadgeView extends View {

    private static final int DEFAULT_TEXT_SIZE = 26;
    private static final int DEFAULT_DOT_SIZE = 20;

    private Paint mPaint;
    private String mText;
    // view绘制的实际宽度
    private float mWidth;
    // view绘制的实际高度
    private float mHeight;
    // 绘制时左右两边弧度对应矩形的宽度
    private float mArcRectWidth;
    // 文字颜色
    private @ColorInt
    int mTextColor = Color.WHITE;
    // 背景颜色
    private @ColorInt
    int mBgColor = Color.RED;
    // 背景颜色
    private @ColorInt
    int mBorderColor = Color.WHITE;
    // 带有弧度的背景path
    private Path mArcPath = new Path();
    // 是否绘制左下角三角
    private boolean mDrawLeftAngle = false;
    // 边框线宽度
    private float mBorderWidth;
    // 纯dot的尺寸
    private float mDotSize = DEFAULT_DOT_SIZE;

    public BadgeView(Context context) {
        this(context, null);
    }

    public BadgeView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BadgeView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
        redraw();
    }

    private void init(Context context) {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.FILL);
        // 设置文本绘制对其方式为center
        mPaint.setTextAlign(Paint.Align.CENTER);
        // 设置默认画笔文字大小
        mPaint.setTextSize(DEFAULT_TEXT_SIZE);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension((int) Math.ceil(mWidth), (int) Math.ceil(mHeight));
    }

    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        if (visibility == VISIBLE) {
            redraw();
        }
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        // 文本为空直接绘制dot
        if (TextUtils.isEmpty(mText)) {
            drawOnlyDot(canvas);
            return;
        }
        // 绘制背景
        drawBg(canvas);
        drawDotText(canvas);
    }

    private void drawOnlyDot(Canvas canvas) {
        mPaint.setColor(mBgColor);
        float radius = mHeight / 2;
        canvas.drawCircle(radius, radius, radius, mPaint);
    }

    private void drawDotText(Canvas canvas) {
        mPaint.setColor(mTextColor);
        // 居中绘制文本
        float baseLineY = mHeight / 2 + Math.abs(mPaint.ascent() + mPaint.descent()) / 2;
        canvas.drawText(mText, mWidth/2, baseLineY, mPaint);
    }

    private void drawBg(Canvas canvas) {
        mPaint.setColor(mBgColor);
        // 绘制边框
        if (mBorderWidth > 0) {
            // 绘制边框时，为了保存弧度一直，mArcRectWidth的值就需要考虑到边框的宽度
            float realArcRectWidth = mArcRectWidth + 2 * mBorderWidth;
            drawColorBg(canvas, realArcRectWidth, 0, mBorderColor);
        }
        // 绘制填充背景
        drawColorBg(canvas, mArcRectWidth, mBorderWidth, mBgColor);
    }

    private void drawColorBg(Canvas canvas, float realArcRectWidth, float offsetWidth, @ColorInt int color) {
        mPaint.setColor(color);
        // 宽高不相等，需要绘制带有弧度的背景，使用path绘制
        mArcPath.reset();
        // 添加左边的弧形，弧形对应的矩形高度为mHeight，宽度为mArcRectWidth，绘制180度
        mArcPath.addArc(new RectF(offsetWidth, offsetWidth, realArcRectWidth + offsetWidth, mHeight - offsetWidth), 90, 180);
        mArcPath.lineTo(mWidth - realArcRectWidth / 2 - offsetWidth, offsetWidth);
        // 添加右边的弧形，与左边弧形一样
        mArcPath.addArc(new RectF(mWidth - realArcRectWidth - offsetWidth, offsetWidth, mWidth - offsetWidth, mHeight - offsetWidth), 270, 180);
        mArcPath.lineTo(realArcRectWidth / 2 + offsetWidth, mHeight - offsetWidth);
        mArcPath.close();
        canvas.drawPath(mArcPath, mPaint);
        // 绘制左下角三角
        if (mDrawLeftAngle) {
            mArcPath.reset();
            mArcPath.moveTo(offsetWidth, mHeight / 2);
            mArcPath.lineTo(offsetWidth, mHeight - offsetWidth);
            mArcPath.lineTo(mWidth / 2, mHeight - offsetWidth);
            mArcPath.close();
            canvas.drawPath(mArcPath, mPaint);
        }
    }

    /**
     * 重新绘制
     */
    private void redraw() {
        // 文本的高度，也就是当前view的高度
        mHeight = getFontHeight();
        // 文本为空，只能绘制dot圆点，让宽度等于高度
        if (TextUtils.isEmpty(mText)) {
            // 如果没有对dot的size赋值，直接使用文本高度1/2
            if (mDotSize <= 0) {
                mDotSize = mHeight / 2;
            }
            mWidth = mDotSize;
            mHeight = mDotSize;
            requestLayout();
            return;
        }
        // 计算文本宽度
        float textWidth = mPaint.measureText(mText);

        // （1）如果是单个文字绘制圆形，宽高相等；
        // （2）多个文字绘制带有左右弧度的椭圆，那么宽度必须大于等于高度，为了让效果比较好看，
        // 这里让整体宽度增加高度1/2，且弧度绘制对应的矩形为宽度mArcRectWidth等于文本高度值，
        // 那么view的宽度就要加上mArcRectWidth
        mArcRectWidth = mHeight;
        // 如果宽度小于高度，让宽高相等，看起来不会那么挤
        mWidth = Math.max(textWidth, mHeight);
        boolean singleWord = isSingleWord();
        if (!singleWord) {
            mWidth += mHeight / 2;
        }
        // 因为可以绘制边框，因此宽高都要加上2倍边框才是最终尺寸
        mHeight += 2 * mBorderWidth;
        mWidth += 2 * mBorderWidth;
        requestLayout();
    }

    public BadgeView textSize(float textSize) {
        // 文字大小必须大于0，否则不生效
        if (textSize > 0) {
            mPaint.setTextSize(textSize);
            redraw();
        }
        return this;
    }

    public BadgeView textColor(@ColorInt int color) {
        if (mTextColor != color) {
            mTextColor = color;
            invalidate();
        }
        return this;
    }

    public BadgeView borderColor(@ColorInt int color) {
        if (mBorderColor != color) {
            mBorderColor = color;
            invalidate();
        }
        return this;
    }

    public BadgeView bgColor(@ColorInt int color) {
        if (mBgColor != color) {
            mBgColor = color;
            invalidate();
        }
        return this;
    }

    private float getFontHeight() {
        Paint.FontMetrics fm = mPaint.getFontMetrics();
        return fm.bottom - fm.top;
    }

    private boolean isSingleWord() {
        return mText.length() == 1;
    }

    public BadgeView showLeftAngle(boolean show) {
        if (mDrawLeftAngle != show) {
            mDrawLeftAngle = show;
            invalidate();
        }
        return this;
    }

    public BadgeView borderWidth(float borderWidth) {
        if (mBorderWidth != borderWidth && borderWidth >= 0) {
            mBorderWidth = borderWidth;
            redraw();
        }
        return this;
    }

    public BadgeView text(String text) {
        if (!TextUtils.equals(mText, text)) {
            mText = text;
            redraw();
        }
        return this;
    }

    public BadgeView onlyDotSize(float size) {
        if (mDotSize != size && size >= 0) {
            mDotSize = size;
            redraw();
        }
        return this;
    }

    public BadgeView show(boolean show) {
        setVisibility(show ? VISIBLE : GONE);
        return this;
    }
}
