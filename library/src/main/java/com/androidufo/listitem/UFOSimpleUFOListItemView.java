package com.androidufo.listitem;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.Nullable;

/**
 * @ClassName UFOSimpleListItemView
 * @Description TODO
 * @Author admin
 * @Date 2021/4/16 9:33
 */
public class UFOSimpleUFOListItemView extends UFOListItemView<UFOSimpleUFOListItemView> {

    public UFOSimpleUFOListItemView(Context context) {
        this(context, null);
    }

    public UFOSimpleUFOListItemView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public UFOSimpleUFOListItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    void createChildren(Context context) {
        createIconView(context);
        createTitleView(context);
        createDescView(context);
        createArrowView(context);
    }

}
