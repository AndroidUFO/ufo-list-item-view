package com.androidufo.listitem.drawable;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * @ClassName ArrowDrawable
 * @Description TODO
 * @Author admin
 * @Date 2021/4/16 12:09
 */
public class ArrowDrawable extends Drawable {
    private static final int DEFAULT_LINE_STROKE = 4;

    protected final Paint mPaint;
    protected RectF mRectF;
    protected float mCenterX;
    protected float mCenterY;
    protected float mDrawFrameSize;
    protected int mLineStroke = DEFAULT_LINE_STROKE;
    protected boolean mRoundLine = true;
    private final Path mPath = new Path();

    public ArrowDrawable() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(mLineStroke);
        mPaint.setStrokeCap(mRoundLine ? Paint.Cap.ROUND : Paint.Cap.SQUARE);
    }

    @Override
    public void setBounds(int left, int top, int right, int bottom) {
        super.setBounds(left, top, right, bottom);
        mRectF = new RectF(left, top, right, bottom);
        mCenterX = mRectF.centerX();
        mCenterY = mRectF.centerY();
        mDrawFrameSize = Math.min(mRectF.width(), mRectF.height());
    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        float half = mDrawFrameSize / 2.0f - mLineStroke;
        float edgeSize = (float) (half / Math.tan(Math.toRadians(90) / 2.0f));
        mPath.reset();
        float startX = mCenterX - edgeSize / 2;
        float endX = mCenterX + edgeSize / 2;
        float startY = mCenterY - half;
        float endY = mCenterY + half;
        mPath.moveTo(startX, startY);
        mPath.lineTo(endX, mCenterY);
        mPath.lineTo(startX, endY);
        canvas.drawPath(mPath, mPaint);
    }

    @Override
    public void setAlpha(int alpha) {
        mPaint.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {
        mPaint.setColorFilter(colorFilter);
    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }

    public ArrowDrawable setLineStroke(int lineStroke) {
        if (mLineStroke != lineStroke && lineStroke > 0) {
            mLineStroke = lineStroke;
            mPaint.setStrokeWidth(mLineStroke);
            invalidateSelf();
        }
        return this;
    }

    public ArrowDrawable setRoundLine(boolean roundLine) {
        if (mRoundLine != roundLine) {
            mRoundLine = roundLine;
            mPaint.setStrokeCap(mRoundLine ? Paint.Cap.ROUND : Paint.Cap.SQUARE);
            invalidateSelf();
        }
        return this;
    }

    public ArrowDrawable setColor(@ColorInt int color) {
        mPaint.setColor(color);
        invalidateSelf();
        return this;
    }
}
