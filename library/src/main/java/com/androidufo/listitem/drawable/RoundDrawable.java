package com.androidufo.listitem.drawable;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * @ClassName CircleDrawable
 * @Description TODO
 * @Author admin
 * @Date 2021/4/1 17:35
 */
public class RoundDrawable extends Drawable {

    private final DrawHelper mHelper;

    public RoundDrawable(Drawable drawable) {
        mHelper = new DrawHelper();
        mHelper.setBitmap(Utils.drawableToBitmap(drawable));
    }

    public Bitmap getResBitmap() {
        return mHelper.getBitmap();
    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        mHelper.doDraw(canvas);
    }

    @Override
    public void setAlpha(int alpha) {
        mHelper.getPaint().setAlpha(alpha);
    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {
        mHelper.getPaint().setColorFilter(colorFilter);
    }

    @Override
    public void setBounds(int left, int top, int right, int bottom) {
        super.setBounds(left, top, right, bottom);
        mHelper.calculateSize(left, top, right, bottom);
    }

    @Override
    public int getOpacity() {
        return mHelper.getOpacity();
    }

}
