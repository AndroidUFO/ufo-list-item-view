package com.androidufo.listitem.drawable;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.RectF;
import android.graphics.Shader;

/**
 * @ClassName DrawHelper
 * @Description TODO
 * @Author admin
 * @Date 2021/4/2 13:38
 */
public class DrawHelper {

    private Bitmap mBitmap;

    private Paint mPaint;
    private RectF mRectF = new RectF();
    private float mRadius;
    private float mCenterX;
    private float mCenterY;
    private BitmapShader mBitmapShader;

    public DrawHelper() {
        initPaint();
    }

    private void initPaint() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setFilterBitmap(true);
        mPaint.setDither(true);
    }

    public void setBitmap(Bitmap bitmap) {
        if (mBitmap == bitmap) {
            return;
        }
        mBitmap = bitmap;
        calculateShader(mBitmap);
    }

    public Bitmap getBitmap() {
        return mBitmap;
    }

    public void doDraw(Canvas canvas) {
        drawCircleBitmap(canvas);
    }

    private void drawCircleBitmap(Canvas canvas) {
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setShader(mBitmapShader);
        canvas.drawCircle(mCenterX, mCenterY, mRadius, mPaint);
    }

    public void calculateSize(int left, int top, int right, int bottom) {
        mRectF.set(left, top, right, bottom);
        mCenterX = mRectF.centerX();
        mCenterY = mRectF.centerY();
        // 计算半径值
        mRadius = Math.min(mRectF.width() / 2.0f, mRectF.height() / 2.0f);
        calculateShader(mBitmap);
    }

    private void calculateShader(Bitmap bitmap) {
        if (bitmap == null || mRadius <= 0) {
            mBitmapShader = null;
            return;
        }
        // 缩放bitmap
        int bmWidth = (int) (mRadius * 2.0f);
        int bmHeight = (int) (mRadius * 2.0f);
        Bitmap scaleBitmap = Utils.scaleBitmapCenterCrop(bitmap, bmWidth, bmHeight);
        if (scaleBitmap == null) {
            return;
        }
        float offsetX = mCenterX - mRadius;
        float offsetY = mCenterY - mRadius;
        mBitmapShader = getMatrixBitmapShader(scaleBitmap, offsetX, offsetY);
    }

    private BitmapShader getMatrixBitmapShader(Bitmap bitmap, float dx, float dy) {
        BitmapShader shader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        Matrix matrix = new Matrix();
        matrix.postTranslate(dx, dy);
        shader.setLocalMatrix(matrix);
        return shader;
    }

    public Paint getPaint() {
        return mPaint;
    }

    public int getOpacity() {
        return (mBitmap == null || mBitmap.hasAlpha()) ?
                PixelFormat.TRANSLUCENT : PixelFormat.OPAQUE;
    }

}
