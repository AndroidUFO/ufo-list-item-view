package com.androidufo.listitem.drawable;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;

/**
 * @ClassName Utils
 * @Description TODO
 * @Author admin
 * @Date 2021/4/1 16:49
 */
public class Utils {
    public static Bitmap scaleBitmapCenterCrop(Bitmap src, int targetWidth, int targetHeight) {
        Bitmap centerCropBitmap = bitmapCenterCropPart(src, targetWidth, targetHeight);
        if (centerCropBitmap == null) {
            return null;
        }
        return Bitmap.createScaledBitmap(
                centerCropBitmap,
                targetWidth,
                targetHeight,
                true
        );
    }

    // 从原始图中截取用于显示Center_Crop模式的图片
    public static Bitmap bitmapCenterCropPart(Bitmap src, int targetWidth, int targetHeight) {
        if (src == null) {
            return null;
        }
        if (targetWidth <= 0 || targetHeight <= 0) {
            return null;
        }
        int bitmapWidth = src.getWidth();
        int bitmapHeight = src.getHeight();

        float scaleWidth = bitmapWidth * 1.0f / targetWidth;
        float scaleHeight = bitmapHeight * 1.0f / targetHeight;
        // 这里为了匹配ImageView本身显示图片的CENTER_CROP模式显示图片，
        // 就需要等比例去图片中心点去截取一部分，并且经过缩放后，要与View的某一边相等，另外一边等比例截取。
        // 这里计算时候，取比例较小的一边进行截取然后缩放达到相同效果
        float clipBmWidth;
        float clipBmHeight;
        float x;
        float y;
        if (scaleWidth <= scaleHeight) {
            clipBmWidth = bitmapWidth;
            clipBmHeight = targetHeight * clipBmWidth / targetWidth;
            x = 0;
            y = (bitmapHeight - clipBmHeight) / 2.0f;
        } else {
            clipBmHeight = bitmapHeight;
            clipBmWidth = targetWidth * clipBmHeight / targetHeight;
            x = (bitmapWidth - clipBmWidth) / 2.0f;
            y = 0;
        }
        return Bitmap.createBitmap(src, (int) x, (int) y, (int) clipBmWidth, (int) clipBmHeight);
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable == null) {
            return null;
        }
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }
        int width = Math.max(drawable.getIntrinsicWidth(), 2);
        int height = Math.max(drawable.getIntrinsicHeight(), 2);
        Bitmap.Config config = drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565;
        Bitmap bitmap = Bitmap.createBitmap(width, height, config);
        Canvas canvas = new Canvas(bitmap);
        if (drawable instanceof ColorDrawable) {
            canvas.drawColor(((ColorDrawable) drawable).getColor());
        }
        drawable.draw(canvas);
        return bitmap;
    }
}
