package com.androidufo.listitem.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import com.androidufo.listitem.drawable.RoundDrawable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

/**
 * @ClassName IconImageView
 * @Description TODO
 * @Author admin
 * @Date 2021/4/16 10:29
 */
public class IconImageView extends AppCompatImageView {

    private boolean mCircleMode;
    private OnVisibleListener mOnVisibleListener;

    public IconImageView(@NonNull Context context) {
        this(context, null);
    }

    public IconImageView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public IconImageView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setVisibility(GONE);
    }

    @Override
    public void setScaleType(ScaleType scaleType) {
        super.setScaleType(ScaleType.CENTER_CROP);
    }

    @Override
    public ScaleType getScaleType() {
        return ScaleType.CENTER_CROP;
    }

    @Override
    public void setImageDrawable(@Nullable Drawable drawable) {
        super.setImageDrawable(drawable);
        setVisibility(drawable == null ? GONE : VISIBLE);
        updateDrawable();
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        if (bm == null) {
            setImageDrawable(null);
            return;
        }
        super.setImageBitmap(bm);
    }

    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        if (mOnVisibleListener != null) {
            mOnVisibleListener.onVisible(visibility == VISIBLE);
        }
    }

    private void updateDrawable() {
        Drawable drawable = getDrawable();
        if (drawable == null) {
            return;
        }
        if (drawable instanceof RoundDrawable) {
            if (!mCircleMode) {
                Bitmap bitmap = ((RoundDrawable) drawable).getResBitmap();
                if (bitmap != null) {
                    setImageBitmap(bitmap);
                }
            }
        } else {
            if (mCircleMode) {
                setImageDrawable(new RoundDrawable(drawable));
            }
        }
    }

    public void circleMode(boolean circle) {
        if (mCircleMode == circle) {
            return;
        }
        mCircleMode = circle;
        updateDrawable();
    }

    public void setOnVisibleListener(OnVisibleListener onVisibleListener) {
        mOnVisibleListener = onVisibleListener;
    }

    public interface OnVisibleListener {
        void onVisible(boolean visible);
    }
}
