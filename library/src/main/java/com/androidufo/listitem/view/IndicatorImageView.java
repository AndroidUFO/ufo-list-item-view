package com.androidufo.listitem.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import com.androidufo.listitem.drawable.ArrowDrawable;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

/**
 * @ClassName ArrowImageView
 * @Description TODO
 * @Author admin
 * @Date 2021/4/16 10:30
 */
public class IndicatorImageView extends AppCompatImageView {

    private @ColorInt int mColor = Color.BLACK;

    public IndicatorImageView(@NonNull Context context) {
        this(context, null);
    }

    public IndicatorImageView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public IndicatorImageView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void color(@ColorInt int color) {
        if (mColor != color) {
            mColor = color;
            updateDrawableColor();
        }
    }

    private void updateDrawableColor() {
        Drawable drawable = getDrawable();
        if (drawable != null) {
            if (drawable instanceof ArrowDrawable) {
                ((ArrowDrawable) drawable).setColor(mColor);
            }
        }
    }

}
