package com.androidufo.listitem.bg;

import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;

import androidx.annotation.ColorInt;
import androidx.annotation.RequiresApi;

public class DrawableUtils {

    public static GradientDrawable createItemBgDrawable(@ColorInt int defaultColor) {
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(defaultColor);
        return drawable;
    }

    public static GradientDrawable createItemBgDrawable(@ColorInt int defaultColor, float cornerRadius) {
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(defaultColor);
        drawable.setCornerRadius(cornerRadius);
        return drawable;
    }

    public static GradientDrawable createItemBgDrawable(@ColorInt int defaultColor,
                                                        float lTRadius,
                                                        float rTRadius,
                                                        float rBRadius,
                                                        float lBRadius) {
        float[] radii = new float[]{
                lTRadius, lTRadius,
                rTRadius, rTRadius,
                rBRadius, rBRadius,
                lBRadius, lBRadius
        };
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(defaultColor);
        drawable.setCornerRadii(radii);
        return drawable;
    }

    public static StateListDrawable createItemStateDrawable(@ColorInt int defaultColor,
                                                            @ColorInt int pressedColor,
                                                            float lTRadius,
                                                            float rTRadius,
                                                            float rBRadius,
                                                            float lBRadius) {
        GradientDrawable defaultDrawable = createItemBgDrawable(defaultColor, lTRadius, rTRadius, rBRadius, lBRadius);
        GradientDrawable pressedDrawable = createItemBgDrawable(pressedColor, lTRadius, rTRadius, rBRadius, lBRadius);
        return createStateListDrawable(defaultDrawable, pressedDrawable);
    }

    public static StateListDrawable createItemStateDrawable(@ColorInt int defaultColor,
                                                         @ColorInt int pressedColor) {
        GradientDrawable defaultDrawable = createItemBgDrawable(defaultColor);
        GradientDrawable pressedDrawable = createItemBgDrawable(pressedColor);
        return createStateListDrawable(defaultDrawable, pressedDrawable);
    }

    public static StateListDrawable createItemStateDrawable(@ColorInt int defaultColor,
                                                            @ColorInt int pressedColor,
                                                            int cornerRadius) {
        GradientDrawable defaultDrawable = createItemBgDrawable(defaultColor, cornerRadius);
        GradientDrawable pressedDrawable = createItemBgDrawable(pressedColor, cornerRadius);
        return createStateListDrawable(defaultDrawable, pressedDrawable);
    }

    private static StateListDrawable createStateListDrawable(GradientDrawable defaultDrawable, GradientDrawable pressedDrawable) {
        StateListDrawable selector = new StateListDrawable();
        selector.addState(new int[]{android.R.attr.state_pressed}, pressedDrawable);
        selector.addState(new int[]{}, defaultDrawable);
        return selector;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static RippleDrawable createRippleItemDrawable(@ColorInt int defaultColor, @ColorInt int pressedColor){
        return createRippleDrawable(createItemBgDrawable(defaultColor), pressedColor);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static RippleDrawable createRippleItemDrawable(@ColorInt int defaultColor, @ColorInt int pressedColor, int cornerRadius){
        return createRippleDrawable(createItemBgDrawable(defaultColor, cornerRadius), pressedColor);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static RippleDrawable createRippleItemDrawable(@ColorInt int defaultColor,
                                                          @ColorInt int pressedColor,
                                                          float lTRadius,
                                                          float rTRadius,
                                                          float rBRadius,
                                                          float lBRadius){
        return createRippleDrawable(
                createItemBgDrawable(defaultColor, lTRadius, rTRadius, rBRadius, lBRadius),
                pressedColor
        );
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private static RippleDrawable createRippleDrawable(GradientDrawable defaultDrawable, @ColorInt int pressedColor){
        return new RippleDrawable(
                ColorStateList.valueOf(pressedColor),
                defaultDrawable,
                null
        );
    }

}
