package com.androidufo.listitem.bg;

import android.graphics.drawable.GradientDrawable;

public class CommonBgBuilder extends BgBuilder<CommonBgBuilder, GradientDrawable> {
    @Override
    public GradientDrawable build() {
        return DrawableUtils.createItemBgDrawable(
                mDefaultBgColor,
                mLeftTopRadius,
                mRightTopRadius,
                mRightBottomRadius,
                mLeftBottomRadius
        );
    }
}
