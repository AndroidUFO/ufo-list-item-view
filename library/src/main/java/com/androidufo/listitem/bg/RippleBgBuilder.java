package com.androidufo.listitem.bg;

import android.graphics.drawable.RippleDrawable;

import androidx.annotation.ColorInt;

public class RippleBgBuilder extends BgBuilder<RippleBgBuilder, RippleDrawable> {

    private @ColorInt int mPressedBgColor;

    public RippleBgBuilder pressedBgColor(@ColorInt int pressedBgColor) {
        this.mPressedBgColor = pressedBgColor;
        return this;
    }

    @Override
    public RippleDrawable build() {
        return DrawableUtils.createRippleItemDrawable(
                mDefaultBgColor,
                mPressedBgColor,
                mLeftTopRadius,
                mRightTopRadius,
                mRightBottomRadius,
                mLeftBottomRadius
        );
    }
}
