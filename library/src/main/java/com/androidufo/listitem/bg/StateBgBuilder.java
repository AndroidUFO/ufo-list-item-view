package com.androidufo.listitem.bg;

import android.graphics.drawable.StateListDrawable;

import androidx.annotation.ColorInt;

public class StateBgBuilder extends BgBuilder<StateBgBuilder, StateListDrawable> {

    private @ColorInt int mPressedBgColor;

    public StateBgBuilder pressedBgColor(@ColorInt int pressedBgColor) {
        this.mPressedBgColor = pressedBgColor;
        return this;
    }

    @Override
    public StateListDrawable build() {
        return DrawableUtils.createItemStateDrawable(
                mDefaultBgColor,
                mPressedBgColor,
                mLeftTopRadius,
                mRightTopRadius,
                mRightBottomRadius,
                mLeftBottomRadius
        );
    }
}
