package com.androidufo.listitem.bg;

import android.graphics.Color;
import android.graphics.drawable.Drawable;

import androidx.annotation.ColorInt;

public abstract class BgBuilder<R extends BgBuilder<R, T>, T extends Drawable> {
    @ColorInt int mDefaultBgColor = Color.WHITE;
    float mLeftTopRadius;
    float mLeftBottomRadius;
    float mRightTopRadius;
    float mRightBottomRadius;

    public R defaultBgColor(@ColorInt int defaultBgColor) {
        this.mDefaultBgColor = defaultBgColor;
        return (R) this;
    }

    public R cornerRadius(float radius) {
        this.mLeftTopRadius = radius;
        this.mLeftBottomRadius = radius;
        this.mRightBottomRadius = radius;
        this.mRightTopRadius = radius;
        return (R) this;
    }

    public R ltCornerRadius(float radius) {
        this.mLeftTopRadius = radius;
        return (R) this;
    }

    public R lbCornerRadius(float radius) {
        this.mLeftBottomRadius = radius;
        return (R) this;
    }

    public R rtCornerRadius(float radius) {
        this.mRightTopRadius = radius;
        return (R) this;
    }

    public R rbCornerRadius(float radius) {
        this.mRightBottomRadius = radius;
        return (R) this;
    }

    public abstract T build();
}
