package com.androidufo.listitem;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidufo.listitem.bg.BgBuilder;
import com.androidufo.listitem.bg.CommonBgBuilder;
import com.androidufo.listitem.bg.RippleBgBuilder;
import com.androidufo.listitem.bg.StateBgBuilder;
import com.androidufo.listitem.view.IconImageView;
import com.androidufo.listitem.view.IndicatorImageView;
import com.androidufo.listitem.drawable.ArrowDrawable;
import com.androidufo.listitem.alpha.AlphaLinearLayout;
import com.androidufo.listitem.listener.OnItemClickListener;

import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

/**
 * @ClassName ListItemView
 * @Description TODO
 * @Author admin
 * @Date 2021/4/16 10:07
 */
public abstract class UFOListItemView<T extends Item<T>> extends AlphaLinearLayout implements Item<T>, View.OnClickListener {

    protected static final int NOT_SET_SIZE = -1;
    private static final int DEFAULT_ITEM_LEFT_PADDING = 36;
    private static final int DEFAULT_ITEM_RIGHT_PADDING = 27;
    protected static final int DEFAULT_INDICATOR_SIZE = 34;
    protected static final int DEFAULT_MARGIN_1 = 14;
    protected static final int DEFAULT_MARGIN_2 = 12;
    protected static final @ColorInt int DEFAULT_TEXT_COLOR_1 = Color.parseColor("#FF484848");
    protected static final @ColorInt int DEFAULT_TEXT_COLOR_2 = Color.parseColor("#FFABABAB");
    private static final int DEFAULT_TITLE_TEXT_SIZE = 46;
    private static final int DEFAULT_SUB_TITLE_TEXT_SIZE = 30;
    protected static final int DEFAULT_DESC_TEXT_SIZE = 40;
    private static final int DEFAULT_FAST_CLICK_INTERVAL = 700;

    private static final int BG_MODE_STATE = 0;
    private static final int BG_MODE_RIPPLE = 1;

    // 防止快速点击监听器
    private OnItemClickListener<T> mOnItemClickListener;
    // 防止快速点击事件间隔时间，单位毫秒，防止快速重复点击
    private long mFastClickInterval;
    // 上次点击时间戳
    private long mLastClickTime;

    protected IconImageView mIconView;
    protected LinearLayout mTitleFrame;
    protected TextView mTitleView;
    protected TextView mSubTitleView;
    protected TextView mDescView;
    protected IndicatorImageView mIndicatorView;

    public UFOListItemView(Context context) {
        this(context, null);
    }

    public UFOListItemView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public UFOListItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setOrientation(HORIZONTAL);
        setGravity(Gravity.CENTER_VERTICAL);
        createChildren(context);
        TypedArray c = context.obtainStyledAttributes(attrs, R.styleable.UFOListItemView, defStyleAttr, 0);
        // icon
        Drawable iconDrawable = c.getDrawable(R.styleable.UFOListItemView_liv_icon);
        int iconWidth = c.getDimensionPixelSize(R.styleable.UFOListItemView_liv_iconWidth, NOT_SET_SIZE);
        int iconHeight = c.getDimensionPixelSize(R.styleable.UFOListItemView_liv_iconHeight, NOT_SET_SIZE);
        boolean iconCircle = c.getBoolean(R.styleable.UFOListItemView_liv_iconCircleMode, false);
        // margin
        int titleHorizontalMargin = c.getDimensionPixelSize(R.styleable.UFOListItemView_liv_titleHorizontalMargin, DEFAULT_MARGIN_1);
        // title
        String titleText = c.getString(R.styleable.UFOListItemView_liv_title);
        ColorStateList titleColor = c.getColorStateList(R.styleable.UFOListItemView_liv_titleTextColor);
        int titleSize = c.getDimensionPixelSize(R.styleable.UFOListItemView_liv_titleTextSize, DEFAULT_TITLE_TEXT_SIZE);
        // subtitle
        String subTitleText = c.getString(R.styleable.UFOListItemView_liv_subTitle);
        ColorStateList subTitleColor = c.getColorStateList(R.styleable.UFOListItemView_liv_subTitleTextColor);
        int subTitleSize = c.getDimensionPixelSize(R.styleable.UFOListItemView_liv_subTitleTextSize, DEFAULT_SUB_TITLE_TEXT_SIZE);
        // desc
        String descText = c.getString(R.styleable.UFOListItemView_liv_desc);
        ColorStateList descColor = c.getColorStateList(R.styleable.UFOListItemView_liv_descTextColor);
        int descSize = c.getDimensionPixelSize(R.styleable.UFOListItemView_liv_descTextSize, DEFAULT_DESC_TEXT_SIZE);
        // indicator
        int indicatorColor = c.getColor(R.styleable.UFOListItemView_liv_indicatorColor, DEFAULT_TEXT_COLOR_1);
        int indicatorWidth = c.getDimensionPixelSize(R.styleable.UFOListItemView_liv_indicatorWidth, DEFAULT_INDICATOR_SIZE);
        int indicatorHeight = c.getDimensionPixelSize(R.styleable.UFOListItemView_liv_indicatorHeight, DEFAULT_INDICATOR_SIZE);
        int indicatorLeftMargin = c.getDimensionPixelSize(R.styleable.UFOListItemView_liv_indicatorLeftMargin, DEFAULT_MARGIN_2);
        // fastClick
        int interval = c.getInt(R.styleable.UFOListItemView_liv_fastClickInterval, DEFAULT_FAST_CLICK_INTERVAL);
        // bg
        int bgMode = c.getInt(R.styleable.UFOListItemView_liv_bgMode, BG_MODE_STATE);
        int[] cornerRadius = {0, 0, 0, 0};
        int bgCornerRadius = c.getDimensionPixelSize(R.styleable.UFOListItemView_liv_bgCornerRadius, NOT_SET_SIZE);
        if (bgCornerRadius > 0) {
            cornerRadius[0] = bgCornerRadius;
            cornerRadius[1] = bgCornerRadius;
            cornerRadius[2] = bgCornerRadius;
            cornerRadius[3] = bgCornerRadius;
        }
        int bgCornerLTRadius = c.getDimensionPixelSize(R.styleable.UFOListItemView_liv_bgLTCornerRadius, NOT_SET_SIZE);
        if (bgCornerLTRadius >= 0) {
            cornerRadius[0] = bgCornerLTRadius;
        }
        int bgCornerLBRadius = c.getDimensionPixelSize(R.styleable.UFOListItemView_liv_bgLBCornerRadius, NOT_SET_SIZE);
        if (bgCornerLBRadius >= 0) {
            cornerRadius[1] = bgCornerLBRadius;
        }
        int bgCornerRTRadius = c.getDimensionPixelSize(R.styleable.UFOListItemView_liv_bgRTCornerRadius, NOT_SET_SIZE);
        if (bgCornerRTRadius >= 0) {
            cornerRadius[2] = bgCornerRTRadius;
        }
        int bgCornerRBRadius = c.getDimensionPixelSize(R.styleable.UFOListItemView_liv_bgRBCornerRadius, NOT_SET_SIZE);
        if (bgCornerRBRadius >= 0) {
            cornerRadius[3] = bgCornerRBRadius;
        }
        int bgDefaultColor = c.getColor(R.styleable.UFOListItemView_liv_bgDefaultColor, Color.WHITE);
        int bgPressedColor = c.getColor(R.styleable.UFOListItemView_liv_bgPressedColor, NOT_SET_SIZE);
        initItemBg(cornerRadius, bgDefaultColor, bgPressedColor, bgMode);
        c.recycle();
        if (iconDrawable != null) {
            icon(iconDrawable);
        }
        if (titleColor == null) {
            titleTextColor(DEFAULT_TEXT_COLOR_1);
        } else {
            titleTextColor(titleColor);
        }
        if (subTitleColor == null) {
            subTitleTextColor(DEFAULT_TEXT_COLOR_1);
        } else {
            subTitleTextColor(subTitleColor);
        }
        if (descColor == null) {
            descTextColor(DEFAULT_TEXT_COLOR_1);
        } else {
            descTextColor(descColor);
        }
        int leftPadding = getPaddingLeft();
        int rightPadding = getPaddingRight();
        if (leftPadding <= 0) {
            leftPadding = DEFAULT_ITEM_LEFT_PADDING;
        }
        if (rightPadding <= 0) {
            rightPadding = DEFAULT_ITEM_RIGHT_PADDING;
        }
        iconSize(iconWidth, iconHeight)
                .iconCircleMode(iconCircle)
                .titleHorizontalMargin(titleHorizontalMargin)
                .title(titleText)
                .titleTextSize(titleSize)
                .subTitle(subTitleText)
                .subTitleTextSize(subTitleSize)
                .desc(descText)
                .descTextSize(descSize)
                .indicatorColor(indicatorColor)
                .indicatorSize(indicatorWidth, indicatorHeight)
                .indicatorLeftMargin(indicatorLeftMargin)
                .fastClickInterval(interval)
                .horizontalPadding(leftPadding, rightPadding);
    }

    private void initItemBg(int[] cornerRadius, int bgDefaultColor, int bgPressedColor, int bgMode) {
        if (bgPressedColor == NOT_SET_SIZE) {
            // 没有设置PressedColor直接使用alpha模式
            itemBg(new CommonBgBuilder()
                    .defaultBgColor(bgDefaultColor)
                    .ltCornerRadius(cornerRadius[0])
                    .lbCornerRadius(cornerRadius[1])
                    .rtCornerRadius(cornerRadius[2])
                    .rbCornerRadius(cornerRadius[3]));

        } else {
            if (bgMode == BG_MODE_STATE) {
                itemBg(new StateBgBuilder()
                        .defaultBgColor(bgDefaultColor)
                        .pressedBgColor(bgPressedColor)
                        .ltCornerRadius(cornerRadius[0])
                        .lbCornerRadius(cornerRadius[1])
                        .rtCornerRadius(cornerRadius[2])
                        .rbCornerRadius(cornerRadius[3]));

            } else {
                itemBg(new RippleBgBuilder()
                        .defaultBgColor(bgDefaultColor)
                        .pressedBgColor(bgPressedColor)
                        .ltCornerRadius(cornerRadius[0])
                        .lbCornerRadius(cornerRadius[1])
                        .rtCornerRadius(cornerRadius[2])
                        .rbCornerRadius(cornerRadius[3]));
            }
        }

    }

    abstract void createChildren(Context context);

    protected void createIconView(Context context) {
        mIconView = new IconImageView(context);
        mIconView.setLayoutParams(getWrapLp());
        mIconView.setOnVisibleListener(new IconImageView.OnVisibleListener() {
            @Override
            public void onVisible(boolean visible) {
                updateTitleViewMargin(visible);
            }
        });
        addView(mIconView);
    }

    private void updateTitleViewMargin(boolean visible) {
        if (mTitleFrame != null) {
            LayoutParams lp = (LayoutParams) mTitleFrame.getLayoutParams();
            lp.leftMargin = visible ? lp.rightMargin : 0;
            mTitleFrame.setLayoutParams(lp);
        }
    }

    protected void createTitleView(Context context) {
        mTitleFrame = new LinearLayout(context);
        mTitleFrame.setOrientation(VERTICAL);
        mTitleFrame.setGravity(Gravity.CENTER_VERTICAL);
        mTitleFrame.setLayoutParams(getWrapLp());
        addView(mTitleFrame);
        createTitle(context);
        createSubTitle(context);
    }

    private void createTitle(Context context) {
        mTitleView = new TextView(context);
        mTitleView.setSingleLine(true);
        mTitleView.setLayoutParams(getWrapLp());
        mTitleFrame.addView(mTitleView);
    }

    private void createSubTitle(Context context) {
        mSubTitleView = new TextView(context);
        mSubTitleView.setSingleLine(true);
        mSubTitleView.setLayoutParams(getWrapLp());
        mSubTitleView.setVisibility(GONE);
        mTitleFrame.addView(mSubTitleView);
    }

    protected void createDescView(Context context) {
        mDescView = new TextView(context);
        mDescView.setGravity(Gravity.RIGHT);
        mDescView.setEllipsize(TextUtils.TruncateAt.END);
        mDescView.setSingleLine(true);
        LayoutParams lp = new LayoutParams(0, LayoutParams.WRAP_CONTENT);
        lp.weight = 1;
        mDescView.setLayoutParams(lp);
        addView(mDescView);
    }

    protected void createArrowView(Context context) {
        mIndicatorView = new IndicatorImageView(context);
        // 默认设置ArrowDrawable
        mIndicatorView.setImageDrawable(new ArrowDrawable());
        mIndicatorView.setLayoutParams(getWrapLp());
        addView(mIndicatorView);
    }

    protected LayoutParams getWrapLp() {
        return new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
    }

    @Override
    public T icon(Drawable drawable) {
        if (mIconView != null) {
            mIconView.setImageDrawable(drawable);
        }
        return (T) this;
    }

    @Override
    public T icon(@DrawableRes int drawable) {
        if (mIconView != null) {
            mIconView.setImageResource(drawable);
        }
        return (T) this;
    }

    @Override
    public T icon(Bitmap bitmap) {
        if (mIconView != null) {
            mIconView.setImageBitmap(bitmap);
        }
        return (T) this;
    }

    @Override
    public T iconSize(int width, int height) {
        if (mIconView != null && width > 0 && height > 0) {
            ViewGroup.LayoutParams lp = mIconView.getLayoutParams();
            lp.width = width;
            lp.height = height;
            mIconView.setLayoutParams(lp);
        }
        return (T) this;
    }

    @Override
    public T iconCircleMode(boolean circle) {
        if (mIconView != null) {
            mIconView.circleMode(circle);
        }
        return (T) this;
    }

    @Override
    public T hideIcon(boolean hide) {
        if (mIconView != null) {
            mIconView.setVisibility(hide ? GONE : VISIBLE);
        }
        return (T) this;
    }

    @Override
    public T titleHorizontalMargin(int margin) {
        if (mTitleFrame != null && margin >= 0) {
            LayoutParams lp = (LayoutParams) mTitleFrame.getLayoutParams();
            lp.rightMargin = margin;
            lp.leftMargin = mIconView.getVisibility() == VISIBLE ? margin : 0;
            mTitleFrame.setLayoutParams(lp);
        }
        return (T) this;
    }

    private void setTitleLeftMargin(int margin) {
        LayoutParams lp = (LayoutParams) mTitleFrame.getLayoutParams();
        lp.leftMargin = margin;
        mTitleFrame.setLayoutParams(lp);
    }

    @Override
    public T title(String title) {
        if (mTitleView != null) {
            mTitleView.setText(title);
        }
        return (T) this;
    }

    @Override
    public T title(@StringRes int title) {
        if (mTitleView != null) {
            mTitleView.setText(title);
        }
        return (T) this;
    }

    @Override
    public T titleTextSize(float size) {
        if (mTitleView != null && size > 0) {
            mTitleView.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
        }
        return (T) this;
    }

    @Override
    public T titleTextColor(@ColorInt int color) {
        if (mTitleView != null) {
            mTitleView.setTextColor(color);
        }
        return (T) this;
    }

    @Override
    public T titleTextColor(ColorStateList colors) {
        if (colors != null) {
            mTitleView.setTextColor(colors);
        }
        return (T) this;
    }

    @Override
    public T subTitle(String subTitle) {
        if (mSubTitleView != null) {
            boolean empty = TextUtils.isEmpty(subTitle);
            mSubTitleView.setText(subTitle);
            mSubTitleView.setVisibility(empty ? GONE : VISIBLE);
        }
        return (T) this;
    }

    @Override
    public T subTitleTextColor(@ColorInt int color) {
        if (mSubTitleView != null) {
            mSubTitleView.setTextColor(color);
        }
        return (T) this;
    }

    @Override
    public T subTitleTextColor(ColorStateList colors) {
        if (colors != null) {
            mSubTitleView.setTextColor(colors);
        }
        return (T) this;
    }

    @Override
    public T subTitleTextSize(float size) {
        if (mSubTitleView != null) {
            mSubTitleView.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
        }
        return (T) this;
    }

    @Override
    public T desc(String desc) {
        if (mDescView != null) {
            mDescView.setText(desc);
        }
        return (T) this;
    }

    @Override
    public T desc(@StringRes int desc) {
        if (mDescView != null) {
            mDescView.setText(desc);
        }
        return (T) this;
    }

    @Override
    public T descTextSize(float size) {
        if (mDescView != null) {
            mDescView.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
        }
        return (T) this;
    }

    @Override
    public T descTextColor(@ColorInt int color) {
        if (mDescView != null) {
            mDescView.setTextColor(color);
        }
        return (T) this;
    }

    @Override
    public T descTextColor(ColorStateList colors) {
        if (colors != null) {
            mDescView.setTextColor(colors);
        }
        return (T) this;
    }

    @Override
    public T indicatorColor(@ColorInt int color) {
        if (mIndicatorView != null) {
            mIndicatorView.color(color);
        }
        return (T) this;
    }

    @Override
    public T indicatorSize(int width, int height) {
        if (mIndicatorView != null && width > 0 && height > 0) {
            ViewGroup.LayoutParams lp = mIndicatorView.getLayoutParams();
            lp.width = width;
            lp.height = height;
            mIndicatorView.setLayoutParams(lp);
        }
        return (T) this;
    }

    @Override
    public T indicatorLeftMargin(int margin) {
        if (mIndicatorView != null && margin >= 0) {
            LayoutParams lp = (LayoutParams) mIndicatorView.getLayoutParams();
            lp.leftMargin = margin;
            mIndicatorView.setLayoutParams(lp);
        }
        return (T) this;
    }

    @Override
    public T hideIndicator(boolean hide) {
        if (mIndicatorView != null) {
            mIndicatorView.setVisibility(hide ? GONE : VISIBLE);
        }
        return (T) this;
    }

    @Override
    public <B extends BgBuilder<B, D>, D extends Drawable> T itemBg(BgBuilder<B, D> bgBuilder) {
        if (bgBuilder != null) {
            setBackground(bgBuilder.build());
            setChangeAlphaWhenPress(bgBuilder instanceof CommonBgBuilder);
        }
        return (T) this;
    }

    @Override
    public T horizontalPadding(int padding) {
        setPadding(padding, getPaddingTop(), padding, getPaddingBottom());
        return (T) this;
    }

    @Override
    public T horizontalPadding(int leftPadding, int rightPadding) {
        setPadding(leftPadding, getPaddingTop(), rightPadding, getPaddingBottom());
        return (T) this;
    }

    //    *****************************防止快速重复点击**********************************
    @Override
    public void onClick(View v) {
        if (isFastClick()) {
            return;
        }
        // 防止重复点击
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onItemClick((T) this);
        }
    }

    /**
     * 是否快速双击
     */
    private boolean isFastClick() {
        long time = SystemClock.uptimeMillis();
        if (time - mLastClickTime < mFastClickInterval) {
            return true;
        }
        mLastClickTime = time;
        return false;
    }

    /**
     * 获取上次防止快速点击生效的点击时间
     */
    public long getLastClickTime() {
        return mLastClickTime;
    }

    /**
     * 设置快速点击间隔，单位毫秒
     * @param interval：毫秒时间值
     */
    @Override
    public T fastClickInterval(long interval) {
        mFastClickInterval = interval;
        return (T) this;
    }

    /**
     * 设置防止快速点击的点击事件监听器
     */
    @Override
    public T setOnItemClickListener(OnItemClickListener<T> listener) {
        mOnItemClickListener = listener;
        if (mOnItemClickListener != null) {
            setOnClickListener(this);
        }
        return (T) this;
    }

    public IconImageView getIconView() {
        return mIconView;
    }
}
