package com.androidufo.listitem;

import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import com.androidufo.listitem.bg.BgBuilder;
import com.androidufo.listitem.listener.OnItemClickListener;

import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;

/**
 * @ClassName Item
 * @Description TODO
 * @AuthoT admin
 * @Date 2021/4/16 9:34
 */
public interface Item<T extends Item<T>> {

    T icon(Drawable drawable);
    T icon(@DrawableRes int drawable);
    T icon(Bitmap bitmap);
    T iconSize(int width, int height);
    T iconCircleMode(boolean circle);
    T hideIcon(boolean hide);

    T titleHorizontalMargin(int margin);

    T title(String title);
    T title(@StringRes int title);
    T titleTextSize(float size);
    T titleTextColor(@ColorInt int color);
    T titleTextColor(ColorStateList colors);

    T subTitle(String subTitle);
    T subTitleTextSize(float size);
    T subTitleTextColor(@ColorInt int color);
    T subTitleTextColor(ColorStateList colors);

    T desc(String desc);
    T desc(@StringRes int desc);
    T descTextSize(float size);
    T descTextColor(@ColorInt int color);
    T descTextColor(ColorStateList colors);

    T indicatorColor(@ColorInt int color);
    T indicatorSize(int width, int height);
    T indicatorLeftMargin(int gap);
    T hideIndicator(boolean hide);

    T fastClickInterval(long interval);
    T setOnItemClickListener(OnItemClickListener<T> listener);

    <B extends BgBuilder<B, D>, D extends Drawable> T itemBg(BgBuilder<B, D> bgBuilder);
    T horizontalPadding(int leftPadding, int rightPadding);
    T horizontalPadding(int padding);

}
