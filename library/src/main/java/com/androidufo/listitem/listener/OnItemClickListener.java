package com.androidufo.listitem.listener;

/**
 * @ClassName OnItemClickListener
 * @Description TODO
 * @Author admin
 * @Date 2021/4/16 13:15
 */
public interface OnItemClickListener<T> {
    void onItemClick(T listItemView);
}
