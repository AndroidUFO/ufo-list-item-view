package com.androidufo.listitem.alpha;

import android.view.View;

import androidx.annotation.NonNull;

public class AlphaViewHelper {

    private View mTarget;

    /**
     * 设置是否要在 press 时改变透明度
     */
    private boolean mChangeAlphaWhenPress = true;

    /**
     * 设置是否要在 disabled 时改变透明度
     */
    private boolean mChangeAlphaWhenDisable = true;

    /**
     * 正常状态的alpha值
     */
    private final float mNormalAlpha = 1f;
    /**
     * 按下去时的alpha值
     */
    private float mPressedAlpha = 0.65f;
    /**
     * 不可用时的alpha值
     */
    private float mDisabledAlpha = 0.5f;

    public AlphaViewHelper(@NonNull View target) {
        mTarget = target;
    }

    public void setPressedAlpha(float pressedAlpha) {
        this.mPressedAlpha = pressedAlpha;
    }

    public void setDisabledAlpha(float disabledAlpha) {
        this.mDisabledAlpha = disabledAlpha;
    }

    void onPressedChanged(View target, boolean pressed) {
        if (mTarget.isEnabled()) {
            mTarget.setAlpha(mChangeAlphaWhenPress && pressed && target.isClickable() ? mPressedAlpha : mNormalAlpha);
        } else {
            if (mChangeAlphaWhenDisable) {
                target.setAlpha(mDisabledAlpha);
            }
        }
    }

    void onEnabledChanged(View target, boolean enabled) {
        float alphaForIsEnable;
        if (mChangeAlphaWhenDisable) {
            alphaForIsEnable = enabled ? mNormalAlpha : mDisabledAlpha;
        } else {
            alphaForIsEnable = mNormalAlpha;
        }
        target.setAlpha(alphaForIsEnable);
    }

    /**
     * 设置是否要在 press 时改变透明度
     *
     * @param changeAlphaWhenPress 是否要在 press 时改变透明度
     */
    public void setChangeAlphaWhenPress(boolean changeAlphaWhenPress) {
        mChangeAlphaWhenPress = changeAlphaWhenPress;
    }

    /**
     * 设置是否要在 disabled 时改变透明度
     *
     * @param changeAlphaWhenDisable 是否要在 disabled 时改变透明度
     */
    public void setChangeAlphaWhenDisable(boolean changeAlphaWhenDisable) {
        mChangeAlphaWhenDisable = changeAlphaWhenDisable;
        onEnabledChanged(mTarget, mTarget.isEnabled());
    }

}
