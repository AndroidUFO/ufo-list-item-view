package com.androidufo.listitem;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.TextView;

import com.androidufo.listitem.badge.BadgeFrameView;
import com.androidufo.listitem.badge.BadgeUtils;
import com.androidufo.listitem.badge.BadgeView;
import com.androidufo.listitem.view.IconImageView;

import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;

/**
 * @ClassName UFOComplexListItemView
 * @Description TODO
 * @Author admin
 * @Date 2021/4/16 9:34
 */
public class UFOComplexListItemView extends UFOListItemView<UFOComplexListItemView> {

    private static final int DEFAULT_PIC_SIZE = 84;

    private BadgeView mLeftDotView;
    private BadgeView mRightDotView;
    private TextView mPicTextView;
    private IconImageView mPicImageView;
    private BadgeFrameView mPicImageFrame;

    public UFOComplexListItemView(Context context) {
        this(context, null);
    }

    public UFOComplexListItemView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public UFOComplexListItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray c = context.obtainStyledAttributes(attrs, R.styleable.UFOComplexListItemView, defStyleAttr, 0);
        int picImageWidth = c.getDimensionPixelSize(R.styleable.UFOComplexListItemView_liv_complex_picImageWidth, DEFAULT_PIC_SIZE);
        int picImageHeight = c.getDimensionPixelSize(R.styleable.UFOComplexListItemView_liv_complex_picImageHeight, DEFAULT_PIC_SIZE);
        int picImageLeftMargin = c.getDimensionPixelSize(R.styleable.UFOComplexListItemView_liv_complex_picImageLeftMargin, DEFAULT_MARGIN_1);
        boolean picImageCircle = c.getBoolean(R.styleable.UFOComplexListItemView_liv_complex_picImageCircleMode, false);
        int badgeTextSize = c.getDimensionPixelSize(R.styleable.UFOComplexListItemView_liv_complex_picImageBadgeTextSize, NOT_SET_SIZE);
        int badgeColor = c.getColor(R.styleable.UFOComplexListItemView_liv_complex_picImageBadgeColor, Color.RED);
        int badgeTextColor = c.getColor(R.styleable.UFOComplexListItemView_liv_complex_picImageBadgeTextColor, Color.WHITE);
        int picTextSize = c.getDimensionPixelSize(R.styleable.UFOComplexListItemView_liv_complex_picTextSize, DEFAULT_DESC_TEXT_SIZE);
        int picTextLeftMargin = c.getDimensionPixelSize(R.styleable.UFOComplexListItemView_liv_complex_picTextLeftMargin, DEFAULT_MARGIN_2);
        ColorStateList picTextColor = c.getColorStateList(R.styleable.UFOComplexListItemView_liv_complex_picTextColor);
        int dotSize = c.getDimensionPixelSize(R.styleable.UFOComplexListItemView_liv_complex_dot_size, NOT_SET_SIZE);
        int rightDotColor = c.getColor(R.styleable.UFOComplexListItemView_liv_complex_rightDotColor, Color.RED);
        int leftDotColor = c.getColor(R.styleable.UFOComplexListItemView_liv_complex_leftDotColor, Color.RED);
        int rightDotLeftMargin = c.getDimensionPixelSize(R.styleable.UFOComplexListItemView_liv_complex_rightDotLeftMargin, DEFAULT_MARGIN_2);
        int rightDotTextSize = c.getDimensionPixelSize(R.styleable.UFOComplexListItemView_liv_complex_rightDotTextSize, NOT_SET_SIZE);
        int rightDotTextColor = c.getColor(R.styleable.UFOComplexListItemView_liv_complex_rightDotTextColor, Color.WHITE);
        c.recycle();
        if (badgeTextSize != NOT_SET_SIZE) {
            picImageBadgeTextSize(badgeTextSize);
        }
        if (dotSize != NOT_SET_SIZE) {
            picImageBadgeDotSize(dotSize);
            leftDotSize(dotSize);
            rightDotSize(dotSize);
        }
        if (picTextColor != null) {
            picTextColor(picTextColor);
        } else {
            picTextColor(DEFAULT_TEXT_COLOR_2);
        }
        if (rightDotTextSize != NOT_SET_SIZE) {
            rightDotTextSize(rightDotTextSize);
        }
        picImageSize(picImageWidth, picImageHeight)
                .picImageBadgeTextColor(badgeTextColor)
                .picImageBadgeColor(badgeColor)
                .picImageLeftMargin(picImageLeftMargin)
                .picImageCircleMode(picImageCircle)
                .picTextSize(picTextSize)
                .rightDotColor(rightDotColor)
                .rightDotLeftMargin(rightDotLeftMargin)
                .rightDotTextColor(rightDotTextColor)
                .leftDotColor(leftDotColor)
                .picTextLeftMargin(picTextLeftMargin);
    }

    public UFOComplexListItemView picImageBadgeTextSize(float size) {
        mPicImageFrame.getBadgeView().textSize(size);
        return this;
    }

    public UFOComplexListItemView picImageBadgeTextColor(@ColorInt int color) {
        mPicImageFrame.getBadgeView().textColor(color);
        return this;
    }

    public UFOComplexListItemView picImageBadgeColor(@ColorInt int color) {
        mPicImageFrame.getBadgeView().bgColor(color);
        return this;
    }

    public UFOComplexListItemView picImageBadgeDotSize(float size) {
        mPicImageFrame.getBadgeView().onlyDotSize(size);
        return this;
    }

    @Override
    void createChildren(Context context) {
        createIconView(context);
        createTitleView(context);
        createLeftDotView(context);
        createDescView(context);
        createPicImageView(context);
        createPicTextView(context);
        createRightDotView(context);
        createArrowView(context);
    }

    private void createPicImageView(Context context) {
        mPicImageView = new IconImageView(context);
        mPicImageView.setOnVisibleListener(new IconImageView.OnVisibleListener() {
            @Override
            public void onVisible(boolean visible) {
                mPicImageFrame.setVisibility(visible ? VISIBLE : GONE);
                mPicImageFrame.getBadgeView().show(visible);
            }
        });
        addView(mPicImageView, getWrapLp());
        mPicImageFrame = BadgeUtils.bindBadge(mPicImageView);
        mPicImageFrame.setVisibility(GONE);
    }

    private void createPicTextView(Context context) {
        mPicTextView = new TextView(context);
        mPicTextView.setEllipsize(TextUtils.TruncateAt.END);
        mPicTextView.setSingleLine(true);
        mPicTextView.setLayoutParams(getWrapLp());
        addView(mPicTextView);
    }

    private void createLeftDotView(Context context) {
        mLeftDotView = new BadgeView(context);
        mLeftDotView.setLayoutParams(getWrapLp());
        mLeftDotView.show(false);
        addView(mLeftDotView);
    }

    private void createRightDotView(Context context) {
        mRightDotView = new BadgeView(context);
        LayoutParams lp = getWrapLp();
        lp.leftMargin = DEFAULT_MARGIN_1;
        mRightDotView.setLayoutParams(lp);
        mRightDotView.show(false);
        addView(mRightDotView);
    }

    public IconImageView getPicImageView() {
        return mPicImageView;
    }

    public UFOComplexListItemView picImageSize(int width, int height) {
        if (width > 0 && height > 0) {
            ViewGroup.LayoutParams lp = mPicImageView.getLayoutParams();
            lp.width = width;
            lp.height = height;
            mPicImageView.setLayoutParams(lp);
        }
        return this;
    }

    public UFOComplexListItemView picImage(Drawable drawable) {
        mPicImageView.setImageDrawable(drawable);
        return this;
    }

    public UFOComplexListItemView picImage(@DrawableRes int drawable) {
        mPicImageView.setImageResource(drawable);
        return this;
    }

    public UFOComplexListItemView picImage(Bitmap bitmap) {
        mPicImageView.setImageBitmap(bitmap);
        return this;
    }

    public UFOComplexListItemView picImageLeftMargin(int margin) {
        if (margin >= 0) {
            LayoutParams lp = (LayoutParams) mPicImageFrame.getLayoutParams();
            lp.leftMargin = margin;
            mPicImageFrame.setLayoutParams(lp);
        }
        return this;
    }

    public UFOComplexListItemView picImageCircleMode(boolean circle) {
        mPicImageView.circleMode(circle);
        return this;
    }

    public UFOComplexListItemView picImageBadgeText(String text) {
        mPicImageFrame.badgeText(text);
        return this;
    }

    public UFOComplexListItemView picImageBadgeShowAngle(boolean show) {
        mPicImageFrame.getBadgeView().showLeftAngle(show);
        return this;
    }

    public UFOComplexListItemView picText(String text) {
        mPicTextView.setText(text);
        mPicTextView.setVisibility(TextUtils.isEmpty(text) ? GONE : VISIBLE);
        return this;
    }

    public UFOComplexListItemView picTextSize(float size) {
        if (size > 0) {
            mPicTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
        }
        return this;
    }

    public UFOComplexListItemView picTextColor(@ColorInt int color) {
        mPicTextView.setTextColor(color);
        return this;
    }

    public UFOComplexListItemView picTextColor(ColorStateList colors) {
        mPicTextView.setTextColor(colors);
        return this;
    }

    public UFOComplexListItemView picTextLeftMargin(int margin) {
        LayoutParams lp = (LayoutParams) mPicTextView.getLayoutParams();
        lp.leftMargin = margin;
        mPicTextView.setLayoutParams(lp);
        return this;
    }

    public UFOComplexListItemView rightDotSize(float size) {
        mRightDotView.onlyDotSize(size);
        return this;
    }

    public UFOComplexListItemView rightDotColor(@ColorInt int color) {
        mRightDotView.bgColor(color);
        return this;
    }

    public UFOComplexListItemView showRightDot(boolean show) {
        mRightDotView.show(show);
        return this;
    }

    public UFOComplexListItemView rightDotLeftMargin(int margin) {
        if (margin >= 0) {
            LayoutParams lp = (LayoutParams) mRightDotView.getLayoutParams();
            lp.leftMargin = margin;
            mRightDotView.setLayoutParams(lp);
        }
        return this;
    }

    public UFOComplexListItemView rightDotTextSize(float size) {
        mRightDotView.textSize(size);
        return this;
    }

    public UFOComplexListItemView rightDotTextColor(@ColorInt int color) {
        mRightDotView.textColor(color);
        return this;
    }

    public UFOComplexListItemView rightDotText(String text) {
        mRightDotView.text(text);
        return this;
    }

    public UFOComplexListItemView leftDotSize(float size) {
        mLeftDotView.onlyDotSize(size);
        return this;
    }

    public UFOComplexListItemView leftDotColor(@ColorInt int color) {
        mLeftDotView.bgColor(color);
        return this;
    }

    public UFOComplexListItemView showLeftDot(boolean show) {
        mLeftDotView.show(show);
        return this;
    }
}
